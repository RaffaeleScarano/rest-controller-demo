package com.example.accessingdatamysql;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;
	
	public Stream<User> getAllUsers(){
		List<User> users= new LinkedList<>();
		userRepository.findAll().forEach(users::add);
		
		return users.stream();
	}
	
	public User getByID(int id) {
		List<User> users= new LinkedList<>();
		userRepository.findById(id).ifPresent(users::add);
		return users.get(0);
	}
	public User addUser(User user) {
		return userRepository.save(user);
	}
	
	public  User updateUser(User user) {
		if (user.getId() != null) {
			Optional<User> fetchedUser = userRepository.findById(user.getId());
			if (fetchedUser.isPresent()) {
				User newUser = fetchedUser.get();
				newUser.setPassword(user.getPassword());
				newUser.setUsername(user.getUsername());
				userRepository.save(user);
				
			}
			return user;
		}
		return null;

		
	}
	
	public  void deleteUser(int id) {
		userRepository.deleteById(id);
	}
	

}
